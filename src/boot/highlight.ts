import hljs from 'highlight.js/lib/core';
import sql from 'highlight.js/lib/languages/sql';
import 'highlight.js/styles/github.css';

hljs.registerLanguage('sql', sql)

console.log('registered highlight.js')

export default () => {
  console.log('running highlight boot script')
}
