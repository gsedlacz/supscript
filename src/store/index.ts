// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {store} from 'quasar/wrappers'
import {InjectionKey} from 'vue'
import {createStore, Store as VuexStore, useStore as vuexUseStore,} from 'vuex'
import {SupScriptModel} from 'src/store/module-script/SupScriptModel';
import scriptModule from 'src/store/module-script';

// import example from './module-example'
// import { ExampleStateInterface } from './module-example/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export interface ApplicationModel {
  // Define your own store structure, using submodules if needed
  script: SupScriptModel;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
}

// provide typings for `this.$store`
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: VuexStore<ApplicationModel>
  }
}

// provide typings for `useStore` helper
export const storeKey: InjectionKey<VuexStore<ApplicationModel>> = Symbol('vuex-key')

// eslint-disable-next-line @typescript-eslint/no-unsafe-call
export default store(function (/* { ssrContext } */) {
  return createStore<ApplicationModel>({
    modules: {
      script: scriptModule
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING
  });
})

export function useStore() {
  return vuexUseStore(storeKey)
}
