import { GetterTree } from 'vuex';
import { ApplicationModel } from '../index';
import {SqlStatement, SupScriptModel} from 'src/store/module-script/SupScriptModel';
import {DB, PLATFORM} from 'components/models';

const getters: GetterTree<SupScriptModel, ApplicationModel> = {
  getDbType (context: SupScriptModel): DB {
    return context.dbType
  },

  getPlatform (context: SupScriptModel): PLATFORM {
    return context.platform
  },

  sqlStatements (context: SupScriptModel): SqlStatement[] {
    return context.sqlStatements
  }
};

export default getters;
