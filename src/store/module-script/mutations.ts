import { MutationTree } from 'vuex';
import {NewSqlStatement, SqlStatement, SupScriptModel, UpdateFileName, UpdateSqlText} from 'src/store/module-script/SupScriptModel';
import {DB, PLATFORM} from 'components/models';

let idGen = 0

const mutation: MutationTree<SupScriptModel> = {
  updateDBSelection ( state: SupScriptModel, dbType: DB) {
    state.dbType = dbType
  },


  updatePlatformSelection ( state: SupScriptModel, platform: PLATFORM) {
    state.platform = platform
  },


  addSqlStatement(state: SupScriptModel, sqlStatement: NewSqlStatement) {
    const sql = {
      id: idGen++,
      sql: sqlStatement.sql,
      outputFileName: sqlStatement.outputFileName
    } as SqlStatement;

    state.sqlStatements.push(sql)
  },

  updateSqlText(state: SupScriptModel, update: UpdateSqlText) {
    const index = state.sqlStatements.findIndex(statement => statement.id === update.id);
    state.sqlStatements[index].sql = update.sql
  },

  updateFileName(state: SupScriptModel, update: UpdateFileName) {
    const index = state.sqlStatements.findIndex(statement => statement.id === update.id);
    state.sqlStatements[index].outputFileName = update.fileName
  },

  removeStatement(state: SupScriptModel, index: number) {
    state.sqlStatements = state.sqlStatements.filter((statement: SqlStatement) =>
      statement.id !== index)
  }
};

export default mutation;
