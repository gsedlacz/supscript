import { ActionTree } from 'vuex';
import { ApplicationModel } from '../index';
import {SupScriptModel} from 'src/store/module-script/SupScriptModel';

const actions: ActionTree<SupScriptModel, ApplicationModel> = {
  someAction (/* context */) {
    // your code
  }
};

export default actions;
