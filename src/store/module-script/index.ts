import { Module } from 'vuex';
import { ApplicationModel } from '../index';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import {SupScriptModel} from 'src/store/module-script/SupScriptModel';
import state from 'src/store/module-script/state';

const scriptModule: Module<SupScriptModel, ApplicationModel> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state
};

export default scriptModule;
