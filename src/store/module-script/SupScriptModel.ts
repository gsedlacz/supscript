import {DB, PLATFORM} from 'components/models';

interface UpdateSqlText {
  id: number
  sql: string
}

interface UpdateFileName {
  id: number
  fileName: string
}


interface NewSqlStatement {
  outputFileName?: string
  sql?: string
}

interface SqlStatement extends NewSqlStatement {
  id: number
  sql: string
}

interface SupScriptModel {
  platform: PLATFORM
  dbType: DB
  sqlStatements: SqlStatement[]
}

export {
  UpdateFileName,
  UpdateSqlText,
  NewSqlStatement,
  SqlStatement,
  SupScriptModel
}
