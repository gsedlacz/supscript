import {SupScriptModel} from 'src/store/module-script/SupScriptModel';
import {DB, PLATFORM} from 'components/models';

function state(): SupScriptModel {
  return {
    platform: PLATFORM.LINUX,
    dbType: DB.MYSQL,
    sqlStatements: []
  }
}

export default state;
