import {SupScriptModel} from 'src/store/module-script/SupScriptModel';
import JSZip, {JSZipFileOptions} from 'jszip';
import {saveAs} from 'file-saver';
import {PLATFORM} from 'components/models';
import {getRunScript, getSqlFileName} from 'src/zip/ScriptBuilder';

export function createZip (sqlModel: SupScriptModel) {
  const zip = new JSZip()

  function addRunSH() {
    const runScript = getRunScript(sqlModel);

    let options
    if(sqlModel.platform === PLATFORM.LINUX) {
      options = {
        unixPermissions: 740,
      } as JSZipFileOptions
    }

    zip.file(
      runScript.name,
      runScript.content,
      options)
  }

  function addSqlFiles() {
    sqlModel.sqlStatements.forEach(statement => {
      if (statement.sql) {
        zip.file(
          getSqlFileName(statement),
          statement.sql)
      }
    })
  }

  function downloadZip() {
    void zip.generateAsync({
      type: 'blob'
    })
    .then(content => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      saveAs(
        content,
        `support_${sqlModel.platform.toLowerCase()}_${sqlModel.dbType.toLowerCase()}.zip`)
    })
  }


  zip.folder('results')
  addRunSH();
  addSqlFiles();
  downloadZip();
}
