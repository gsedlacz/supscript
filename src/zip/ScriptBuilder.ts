import {SqlStatement, SupScriptModel} from 'src/store/module-script/SupScriptModel';
import {DB, PLATFORM} from 'components/models';

interface RunScript {
  name: string
  content: string
}

interface ScriptBuilder {
  _getScriptName: () => string
  _createContentHeader: () => string
  _createSqlExecutionLine: (statement: SqlStatement) => string

  createRunScript: (statements: SqlStatement[]) => RunScript
}

abstract class BaseScriptBuilder implements ScriptBuilder {
  abstract _createSqlExecutionLine(statement: SqlStatement): string

  abstract _createContentHeader(): string

  abstract _getScriptName(): string

  _createContent(statements: SqlStatement[]): string {
    let content = this._createContentHeader()
    content += '\n\n\n'
    statements.forEach(statement => {
      if(!statement.sql) return

      content += '\n'
      content += this._createSqlExecutionLine(statement)
    })
    return content
  }

  createRunScript(statements: SqlStatement[]): RunScript {
    return {
      name: this._getScriptName(),
      content: this._createContent(statements)
    };
  }

}

abstract class LinuxBuilder extends BaseScriptBuilder {
  _getScriptName(): string {
    return 'run.sh'
  }
}

class LinuxMysqlScriptBuilder extends LinuxBuilder {
  _createContentHeader(): string {
    return `#!/bin/sh

export Suser=
export Spass=
export Sschema=
export Shost=`
  }

  _createSqlExecutionLine(statement: SqlStatement): string {
    let line = `mysql -h $Shost -u $Suser -p$Spass $Sschema < ${getSqlFileName(statement)}`
    if(statement.outputFileName) {
      line += ` > ./results/${statement.outputFileName}`
    }
    return line
  }
}

class LinuxOracleScriptBuilder extends LinuxBuilder {
  _createContentHeader(): string {
    return `#!/bin/sh

export Suser=
export Spass=
export Sdbcn=`
  }

  _createSqlExecutionLine(statement: SqlStatement): string {
    let line = `sqlplus $Suser/$Spass@$Sdbcn @${getSqlFileName(statement)}`
    if(statement.outputFileName) {
      line += ` > ./results/${statement.outputFileName}`
    }
    return line
  }
}


abstract class WindowsBuilder extends BaseScriptBuilder {
  _getScriptName(): string {
    return 'run.bat'
  }
}

class WindowsMysqlScriptBuilder extends WindowsBuilder {
  _createContentHeader(): string {
    return `set Suser=
set Spass=
set Sschema=
set Shost=`
  }

  _createSqlExecutionLine(statement: SqlStatement): string {
    let line = `mysql -h %Shost% -u %Suser% -p%Spass% %Sschema% < ${getSqlFileName(statement)}`
    if(statement.outputFileName) {
      line += ` > ./results/${statement.outputFileName}`
    }
    return line
  }
}

class WindowsOracleScriptBuilder extends WindowsBuilder {
  _createContentHeader(): string {
    return `set Suser=
set Spass=
set Sdbcn=`
  }

  _createSqlExecutionLine(statement: SqlStatement): string {
    let line = `sqlplus %Suser%/%Spass%@%Sdbcn% @${getSqlFileName(statement)}`
    if(statement.outputFileName) {
      line += ` > ./results/${statement.outputFileName}`
    }
    return line
  }
}


function getRunScript(sqlModel: SupScriptModel): RunScript {
  const platform = sqlModel.platform
  const database = sqlModel.dbType

  let generator: ScriptBuilder

  if (platform === PLATFORM.LINUX) {
    if (database === DB.MYSQL) {
      generator = new LinuxMysqlScriptBuilder()
    } else {
      generator = new LinuxOracleScriptBuilder()
    }

  } else {
    if (database === DB.MYSQL) {
      generator = new WindowsMysqlScriptBuilder()
    } else {
      generator = new WindowsOracleScriptBuilder()
    }
  }

  return generator.createRunScript(sqlModel.sqlStatements)
}

function getSqlFileName(statement: SqlStatement) {
  return String(statement.id) + '.sql';
}


export {
  getSqlFileName,
  getRunScript,
  LinuxMysqlScriptBuilder,
  LinuxOracleScriptBuilder,
  WindowsMysqlScriptBuilder,
  WindowsOracleScriptBuilder
}
