export enum PLATFORM {
  LINUX = 'Linux',
  WINDOWS = 'Windows'
}

const platforms: PLATFORM[] = [PLATFORM.LINUX, PLATFORM.WINDOWS]


export enum DB {
  MYSQL = 'MySQL',
  ORACLE = 'Oracle'
}

const dbTypes: DB[] = [DB.MYSQL, DB.ORACLE] as DB[]


export {
  dbTypes,
  platforms
}
