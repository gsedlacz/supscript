# build stage
FROM node:18-alpine as build-stage

WORKDIR /app

COPY . .

RUN npm i -g @quasar/cli && \
    npm i -g @vue/cli && \
    npm i -g @vue/cli-init && \
    npm i && \
    npm run build-prod


# production stage
FROM nginx:1.21.1-alpine as production-stage

COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
